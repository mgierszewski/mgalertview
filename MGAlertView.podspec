Pod::Spec.new do |s|
	s.name			= "MGAlertView"
	s.version		= "0.0.1"
	s.summary		= "Custom alert view for iOS"
	s.author		= { "Maciej Gierszewski" => "m.gierszewski@futuremind.com" }
	s.platform		= :ios, '5.0'
	s.source		= { :git => "https://bitbucket.org/mgierszewski/mgalertview.git", :tag => "0.0.1" }
	s.frameworks	= 'Accelerate', 'QuartzCore'
	s.source_files	= 'MGAlertView'
	s.requires_arc	= true
	s.license		= { :type => 'WTFPL' }
	s.homepage		= 'https://bitbucket.org/mgierszewski/mgalertview.git'
end
