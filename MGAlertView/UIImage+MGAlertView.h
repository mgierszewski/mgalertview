//
//  UIImage+Blur.h
//
//  Created by Maciej Gierszewski on 24.09.2013.
//  Copyright (c) 2013 Maciej Gierszewski All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (MGAlertView)

- (UIImage *)blurImageWithBoxSize:(int)boxSize; 
+ (UIImage *)imageWithFillColor:(UIColor *)color;

@end
