//
//  MGAlertView+Static.m
//  MGAlertView
//
//  Created by Maciek Gierszewski on 23.07.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import "MGAlertView.h"
#import "MGAlertViewContainerWindow.h"
#import "MGAlertViewController.h"

@implementation MGAlertView (Static)

+ (void)setBlurredBackgroundEnabled:(BOOL)enabled
{
    [MGAlertView setBackgroundBlurType:enabled ? MGAlertViewBackgroundBlurTypeWindow : MGAlertViewBackgroundBlurTypeNone];
}

+ (BOOL)blurredBackgroundEnabled
{
    return [MGAlertView backgroundBlurType] != MGAlertViewBackgroundBlurTypeNone;
}

+ (void)setBackgroundBlurType:(MGAlertViewBackgroundBlurType)type
{
    [[[MGAlertViewContainerWindow sharedInstance] alertViewController] setBackgroundBlurType:type];
}

+ (MGAlertViewBackgroundBlurType)backgroundBlurType
{
    return [[[MGAlertViewContainerWindow sharedInstance] alertViewController] backgroundBlurType];
}

+ (void)setBlurBoxSize:(int)blurBoxSize
{
    [[[MGAlertViewContainerWindow sharedInstance] alertViewController] setBlurBoxSize:blurBoxSize];
}

+ (int)blurBoxSize
{
    return [[[MGAlertViewContainerWindow sharedInstance] alertViewController] blurBoxSize];
}

+ (void)setWindowBackgroundColor:(UIColor *)windowBackgroundColor
{
    [[[MGAlertViewContainerWindow sharedInstance] alertViewController] setBackgroundColor:windowBackgroundColor];
}

+ (UIColor *)windowBackgroundColor
{
    return [[[MGAlertViewContainerWindow sharedInstance] alertViewController] backgroundColor];
}

+ (NSArray *)visibleAlerts
{
    MGAlertViewController *alertViewController = [[MGAlertViewContainerWindow sharedInstance] alertViewController];
    return [alertViewController visibleAlerts];
}

#pragma mark - Static creators

+ (MGAlertView *)showAlertViewWithCustomView:(UIView *)customView
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:nil message:nil customView:customView delegate:nil buttons:nil];
    [alertView show:YES];
    
    return alertView;
}

+ (MGAlertView *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:title message:message customView:nil delegate:nil buttons:@[@"OK"]];
    [alertView show:YES];
    
    return alertView;
}

+ (MGAlertView *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message customView:(UIView *)customView
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:title message:message customView:customView delegate:nil buttons:@[@"OK"]];
    [alertView show:YES];
    
    return alertView;
}

+ (MGAlertView *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message customView:(UIView *)customView delegate:(id<MGAlertViewDelegate>)delegate buttons:(NSArray *)buttons
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:title message:message customView:customView delegate:delegate buttons:buttons];
    [alertView show:YES];
    
    return alertView;
}

+ (MGAlertView *)showAlertViewWithMessage:(NSString *)message customView:(UIView *)customView
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:nil message:message customView:customView delegate:nil buttons:@[@"OK"]];
    [alertView show:YES];
    
    return alertView;
}

+ (MGAlertView *)showAlertViewWithMessage:(NSString *)message
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:nil message:message image:nil delegate:nil buttons:@[@"OK"]];
    [alertView show:YES];
    
    return alertView;
}

+ (MGAlertView *)showAlertViewWithImage:(UIImage *)image
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:nil message:nil image:image delegate:nil buttons:@[@"OK"]];
    [alertView show:YES];
    
    return alertView;
}

+ (MGAlertView *)showAlertViewWithMessage:(NSString *)message image:(UIImage *)image
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:nil message:message image:image delegate:nil buttons:@[@"OK"]];
    [alertView show:YES];
    
    return alertView;
}

+ (MGAlertView *)showAlertViewWithLoader
{
    UIActivityIndicatorView *loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [loader startAnimating];
    
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:nil message:nil customView:loader delegate:nil buttons:nil];
    [alertView show:YES];
    
    return alertView;
}

+ (MGAlertView *)showFlashAlertViewWithImage:(UIImage *)image
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:nil message:nil image:image delegate:nil buttons:nil];
    [alertView show:YES];
    [alertView hide:YES afterDelay:1.0];
    
    return alertView;
}

+ (MGAlertView *)showFlashAlertViewWithMessage:(NSString *)message
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:nil message:message customView:nil delegate:nil buttons:nil];
    [alertView show:YES];
    [alertView hide:YES afterDelay:1.0];
    
    return alertView;
}

+ (MGAlertView *)showFlashAlertViewWithMessage:(NSString *)message image:(UIImage *)image
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:nil message:message image:image delegate:nil buttons:nil];
    [alertView show:YES];
    [alertView hide:YES afterDelay:1.0];
    
    return alertView;
}

+ (MGAlertView *)showFlashAlertViewWithTitle:(NSString *)title message:(NSString *)message
{
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:title message:message image:nil delegate:nil buttons:nil];
    [alertView show:YES];
    [alertView hide:YES afterDelay:1.0];
    
    return alertView;
}

@end