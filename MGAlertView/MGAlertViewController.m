//
//  MGAlertViewController.m
//
//  Created by Maciej Gierszewski on 19.02.2014.
//  Copyright (c) 2014 Maciej Gierszewski All rights reserved.
//

#import "MGAlertView+Animation.h"
#import "MGAlertViewController.h"
#import "MGAlertViewContainerWindow.h"
#import "UIImage+MGAlertView.h"
#import "UIView+MGAlertView.h"
#import "MGAlertViewUtils.h"

CGFloat const MGAlertViewRenderImageScale = 0.5f;
NSTimeInterval const MGAlertViewBackgroundAnimationDuration = 0.2;

@interface MGAlertViewController ()
@property (atomic, assign) BOOL renderingBackground;

@property (nonatomic, assign) CGFloat statusBarOffset;
@property (nonatomic, assign) CGFloat keyboardHeight;

@property (nonatomic, strong) UIImageView *imageView;   // blurred background container
@property (nonatomic, strong) UIView *windowColorView;  // color overlay

@property (nonatomic, strong) NSMutableArray *alerts;
@property (nonatomic, strong) NSMutableArray *alertsBeingRemoved;

- (void)updateStatusOffsetWithStatusBarFrame:(CGRect)statusBarFrame;
- (void)startRefreshingBackground;
- (void)stopRefreshingBackground;
- (void)renderingBackgroundRoutine;
- (void)layoutAlertsWithAnimationDuration:(NSTimeInterval)animationDuration;

- (void)keyboardWillShowNotification:(NSNotification *)notification;
- (void)keyboardWillHideNotification:(NSNotification *)notification;
- (void)statusBarFrameChangedNotification:(NSNotification *)notification;
- (void)textInputWillBeginEditingNotification:(NSNotification *)notification;

- (UIImage *)blurredBackgroundImageInRect:(CGRect)rect;
@end

@implementation MGAlertViewController

- (instancetype)init
{
	self = [super init];
	if (self)
	{
		if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f)
		{
			self.extendedLayoutIncludesOpaqueBars = YES;
		}
		else
		{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
			self.wantsFullScreenLayout = YES;
#pragma clang diagnostic pop
		}
		
		self.renderingBackground = NO;
		
		self.backgroundBlurType = MGAlertViewBackgroundBlurTypeNone;
		self.blurBoxSize = 21;
		
		self.alerts = [NSMutableArray array];
		self.alertsBeingRemoved = [NSMutableArray array];
		self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.3f];
		
		[self updateStatusOffsetWithStatusBarFrame:[[UIApplication sharedApplication] statusBarFrame]];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameChangedNotification:) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textInputWillBeginEditingNotification:) name:UITextFieldTextDidBeginEditingNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textInputWillBeginEditingNotification:) name:UITextViewTextDidBeginEditingNotification object:nil];
	}
	return self;
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadView
{
	// THE view
	CGRect viewBounds = self.parentWindow.bounds;
	
	UIView *view = [[UIView alloc] initWithFrame:viewBounds];
	view.backgroundColor = [UIColor clearColor];
	view.clipsToBounds = YES;
	view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	// blurred background container
	UIImageView *imageView = [[UIImageView alloc] initWithFrame:view.bounds];
	imageView.alpha = 0.0f;
	imageView.backgroundColor = [UIColor clearColor];
	imageView.contentMode = UIViewContentModeScaleToFill;
	imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	// dim color view
	UIView *windowColorView = [[UIView alloc] initWithFrame:view.bounds];
	windowColorView.alpha = 0.0f;
	windowColorView.backgroundColor = self.backgroundColor;
	windowColorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	[view addSubview:imageView];
	[view addSubview:windowColorView];
	
	self.view = view;
	self.imageView = imageView;
	self.windowColorView = windowColorView;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	for (MGAlertView *alertView in self.alerts)
	{
		[alertView setNeedsLayout];
		[UIView animateWithDuration:duration animations:^{
			[alertView layoutIfNeeded];
		}];
	}
}

- (UIViewController *)childViewControllerForStatusBarStyle
{
	UIViewController *visibleViewController = [[[[UIApplication sharedApplication] windows] firstObject] rootViewController];
	return visibleViewController;
}

#pragma mark - Update

- (void)updateStatusOffsetWithStatusBarFrame:(CGRect)statusBarFrame
{
	CGFloat statusBarOffset = MIN(CGRectGetWidth(statusBarFrame), CGRectGetHeight(statusBarFrame)) - 20.0f;
	if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f && statusBarOffset > 0.0f)
	{
		statusBarOffset = 0.0; // the in-call bar is not translucent on iOS 6 and doesn't overlap the window
	}
	
	self.statusBarOffset = MAX(0.0f, statusBarOffset);
	[self layoutAlertsWithAnimationDuration:0.2];
}

#pragma mark - Alert View Management

- (void)layoutAlertsWithAnimationDuration:(NSTimeInterval)animationDuration
{
	for (MGAlertView *alertView in self.visibleAlerts)
	{
		if (CGRectGetWidth(alertView.frame) > 0.0f && CGRectGetHeight(alertView.frame) > 0.0f)
		{
			[alertView setNeedsLayout];
			[UIView animateWithDuration:animationDuration delay:0.0 options:0 animations:^{
				[alertView layoutIfNeeded];
			} completion:^(BOOL finished) {
			}];
		}
	}
}

- (void)addAlertView:(MGAlertView *)alertView
{
	// check if there are no alerts (those currently being removed don't count)
	if ([self.alerts count] == 0)
	{
		[self.parentWindow makeKeyAndVisible];
		
		// dim background
		[UIView animateWithDuration:MGAlertViewBackgroundAnimationDuration
						 animations:^{
							 self.windowColorView.alpha = 1.0f;
						 }];
		
		// blur background
		if (self.backgroundBlurType == MGAlertViewBackgroundBlurTypeWindow)
		{
			self.imageView.alpha = 0.0f;
			self.imageView.image = [self blurredBackgroundImageInRect:self.view.bounds];
			
			[UIView animateWithDuration:MGAlertViewBackgroundAnimationDuration
							 animations:^{
								 self.imageView.alpha = 1.0f;
							 }];
		}
		
		if (self.backgroundBlurType != MGAlertViewBackgroundBlurTypeNone)
		{
			[self startRefreshingBackground];
		}
	}
	
	[self.alerts addObject:alertView];
	[self.view addSubview:alertView];
}

- (void)removeAlertView:(MGAlertView *)alertView
{
	[alertView removeFromSuperview];
	
	if (![self.alerts containsObject:alertView])
	{
		return;
	}
	[self.alerts removeObject:alertView];
	[self.alertsBeingRemoved removeObject:alertView];
	
	if ([self.alerts count] == 0)
	{
		[self.parentWindow resignKeyWindow];
		[self.parentWindow setHidden:YES];
	}
}

- (void)prepareToRemoveAlertView:(MGAlertView *)alertView
{
	if (![self.alerts containsObject:alertView] || [self.alertsBeingRemoved containsObject:alertView])
	{
		return;
	}
	[self.alertsBeingRemoved addObject:alertView];
	if ([self.alerts count] == [self.alertsBeingRemoved count])
	{
		[UIView animateWithDuration:MGAlertViewHideAnimationDuration
						 animations:^{
							 self.windowColorView.alpha = 0.0f;
							 self.imageView.alpha = 0.0f;
						 }];
		
		[self stopRefreshingBackground];
	}
}

#pragma mark - Setters

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
	_backgroundColor = backgroundColor;
	
	if ([self.alerts count] > 0)
	{
		self.windowColorView.alpha = 1.0f;
	}
}

- (void)setBackgroundBlurType:(MGAlertViewBackgroundBlurType)backgroundBlurType
{
	_backgroundBlurType = backgroundBlurType;
	
	if ([self.alerts count] > 0)
	{
		// invalidate
		if (self.backgroundBlurType != MGAlertViewBackgroundBlurTypeWindow)
		{
			self.imageView.image = nil;
			self.imageView.alpha = 0.0f;
		}
		else
		{
			self.imageView.image = [self blurredBackgroundImageInRect:self.view.bounds];
			self.imageView.alpha = 1.0f;
		}
		
		if (self.backgroundBlurType == MGAlertViewBackgroundBlurTypeWindow)
		{
			[self stopRefreshingBackground];
		}
		else
		{
			[self startRefreshingBackground];
		}
	}
}

- (void)setBlurBoxSize:(int)blurBoxSize
{
	_blurBoxSize = blurBoxSize + (1 - blurBoxSize % 2);
}

#pragma mark - Getters

- (NSArray *)visibleAlerts
{
	NSMutableArray *alerts = [self.alerts mutableCopy];
	[alerts removeObjectsInArray:self.alertsBeingRemoved];
	
	return [NSArray arrayWithArray:alerts];
}

#pragma mark - Notification

- (void)statusBarFrameChangedNotification:(NSNotification *)notification
{
	CGRect statusBarFrame = [notification.userInfo[UIApplicationStatusBarFrameUserInfoKey] CGRectValue];
	[self updateStatusOffsetWithStatusBarFrame:statusBarFrame];
}

- (void)keyboardWillHideNotification:(NSNotification *)notification
{
	self.keyboardHeight = 0.0f;
	[self layoutAlertsWithAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
}

- (void)keyboardWillShowNotification:(NSNotification *)notification
{
	CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
	self.keyboardHeight = MIN(CGRectGetWidth(keyboardFrame), CGRectGetHeight(keyboardFrame));
	[self layoutAlertsWithAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
}

- (void)textInputWillBeginEditingNotification:(NSNotification *)notification
{
	if (self.keyboardHeight > 0.0f)
	{
		[self layoutAlertsWithAnimationDuration:0.2];
	}
}

#pragma mark - Blurred background

- (UIImage *)blurredBackgroundImageInRect:(CGRect)rect
{
	@autoreleasepool {
		UIGraphicsBeginImageContextWithOptions(rect.size, NO, MGAlertViewRenderImageScale);
		CGContextRef context = UIGraphicsGetCurrentContext();
		
		if (context == 0)
		{
			UIGraphicsEndImageContext();
			return nil;
		}
		
		CGContextTranslateCTM(context, -CGRectGetMinX(rect), -CGRectGetMinY(rect));
		CGContextClipToRect(context, rect);
		
		// get the screen dimensions
		CGAffineTransform windowTransform = CGAffineTransformIdentity;
		if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0f)
		{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
			CGFloat rotationAngle = 0.0f;
			switch (self.interfaceOrientation)
			{
				case UIInterfaceOrientationPortraitUpsideDown:
					rotationAngle = M_PI;
					break;
					
				case UIInterfaceOrientationLandscapeLeft:
					rotationAngle = M_PI_2;
					break;
					
				case UIInterfaceOrientationLandscapeRight:
					rotationAngle = -M_PI_2;
					break;
					
				case UIInterfaceOrientationPortrait:
				default:
					;
			}
#pragma clang diagnostic pop
			
			UIScreen *mainScreen = [UIScreen mainScreen];
			
			CGAffineTransform rotation = CGAffineTransformMakeRotation(rotationAngle);
			CGSize screenSize = CGRectApplyAffineTransform(mainScreen.bounds, rotation).size;
			
			// rotate the screen accordingly
			CGContextTranslateCTM(context, screenSize.width/2.0f, screenSize.height/2.0f);
			CGContextConcatCTM(context, rotation);
			CGContextTranslateCTM(context, -screenSize.width/2.0f, -screenSize.height/2.0f);
			
			// iOS 7 needed
			windowTransform = rotation;
		}
		
		// render windows below the alert
		NSArray *windows = nil;
		@try
		{
			NSArray *allWindows = [[UIApplication sharedApplication] windows];
			windows = [allWindows filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.hidden == NO AND self.windowLevel < %f", self.parentWindow.windowLevel]];
		}
		@catch (NSException *exception)
		{
			NSLog(@"%@", exception);
		}
		
		[windows enumerateObjectsUsingBlock:^(UIWindow *window, NSUInteger idx, BOOL *stop) {
			
			CGRect windowBounds = window.bounds;
			CGRect transformedWindowBounds = CGRectStandardize(CGRectApplyAffineTransform(windowBounds, windowTransform));
			
			if (CGRectGetWidth(windowBounds) > 0 && CGRectGetHeight(windowBounds) > 0)
			{
				CGAffineTransform transform = CGAffineTransformIdentity;
				transform = CGAffineTransformTranslate(transform,
													   transformedWindowBounds.size.width * window.layer.anchorPoint.x,
													   transformedWindowBounds.size.height * window.layer.anchorPoint.y);
				transform = CGAffineTransformConcat(transform, window.transform);
				transform = CGAffineTransformTranslate(transform,
													   -windowBounds.size.width * window.layer.anchorPoint.x,
													   -windowBounds.size.height * window.layer.anchorPoint.y);
				
				CGContextSaveGState(context);
				CGContextConcatCTM(context, transform);
				
				// Render the layer hierarchy to the current context
				if ([window respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)])
				{
					@try
					{
						[window drawViewHierarchyInRect:window.bounds afterScreenUpdates:NO];
					}
					@catch (NSException *exception)
					{
						NSLog(@"%@", exception);
					}
				}
				else
				{
					@try
					{
						[window.layer renderInContext:context];
					}
					@catch (NSException *exception)
					{
						NSLog(@"%@", exception);
					}
				}
				CGContextRestoreGState(context);
			}
		}];
		
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		// image blur
		UIImage *returnImage = [image blurImageWithBoxSize:self.blurBoxSize];
		return returnImage;
	}
}

- (void)startRefreshingBackground
{
	self.renderingBackground = YES;
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		
		[self renderingBackgroundRoutine];
#ifdef DEBUG
		NSLog(@"MGAlertView stopped rendering background");
#endif
	});
}

- (void)stopRefreshingBackground
{
	self.renderingBackground = NO;
}

- (void)renderingBackgroundRoutine
{
#ifdef MGAlertViewShowFPS
	int frames = 0;
	CFAbsoluteTime timeElapsed = 0.0;
#endif
	while (self.renderingBackground)
	{
		
#ifdef MGAlertViewShowFPS
		CFAbsoluteTime time = MeasureBlock(^{
#endif
			if ([self.alerts count] > 1)
			{
				UIImage *wholeScreenImage = [self blurredBackgroundImageInRect:self.view.bounds];
				
				// alert's frame must be scaled to CGImage actual dimensions
				CGAffineTransform frameTransform = CGAffineTransformMakeScale(MGAlertViewRenderImageScale, MGAlertViewRenderImageScale);
				
				[self.alerts enumerateObjectsUsingBlock:^(MGAlertView *alertView, NSUInteger idx, BOOL *stop) {
					
					if (!CGSizeEqualToSize(alertView.frame.size, CGSizeZero))
					{
						__block CGRect alertFrame;
						dispatch_sync(dispatch_get_main_queue(), ^{
							alertFrame = [[alertView.layer presentationLayer] frame];
						});
						CGRect transformedAlertFrame = CGRectApplyAffineTransform(CGRectOffset(alertFrame, 0.0f, self.statusBarOffset), frameTransform);
						CGImageRef croppedImage = CGImageCreateWithImageInRect(wholeScreenImage.CGImage, transformedAlertFrame);
						
						UIImage *image = [UIImage imageWithCGImage:croppedImage
															 scale:wholeScreenImage.scale
													   orientation:wholeScreenImage.imageOrientation];
						CGImageRelease(croppedImage);
						
						dispatch_sync(dispatch_get_main_queue(), ^{
							alertView.alertBackgroundImage = image;
						});
					}
				}];
			}
			else if ([self.alerts count] > 0)
			{
				// one alert - render once in proper dimension
				MGAlertView *alertView = [self.alerts firstObject];
				if (!CGSizeEqualToSize(alertView.frame.size, CGSizeZero))
				{
					__block CGRect alertFrame = alertView.frame;
					dispatch_sync(dispatch_get_main_queue(), ^{
						alertFrame = [[alertView.layer presentationLayer] frame];
					});
					
					CGRect transformedAlertFrame = CGRectOffset(alertFrame, 0.0f, self.statusBarOffset);
					UIImage *image = [self blurredBackgroundImageInRect:transformedAlertFrame];
					
					dispatch_sync(dispatch_get_main_queue(), ^{
						alertView.alertBackgroundImage = image;
					});
				}
			}
#ifdef MGAlertViewShowFPS
		});
		
		frames++;
		timeElapsed += time;
		
		if (timeElapsed >= 1.0)
		{
			DebugLog(@"FPS %f", frames/timeElapsed);
			frames = 0;
			timeElapsed = 0;
		}
#endif
	}
}

@end
