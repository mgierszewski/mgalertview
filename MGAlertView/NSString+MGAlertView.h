//
//  NSString+MGAlertView.h
//
//  Created by Maciej Gierszewski on 14.04.2014.
//  Copyright (c) 2014 Maciej Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (MGAlertView)
- (CGSize)sizeWithFont:(UIFont *)font maximumWidth:(CGFloat)width;
@end
