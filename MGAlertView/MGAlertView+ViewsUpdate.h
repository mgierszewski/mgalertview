//
//  MGAlertView+ViewsUpdate.h
//  MGAlertView
//
//  Created by Maciek Gierszewski on 22.10.2015.
//  Copyright © 2015 Maciej Gierszewski. All rights reserved.
//

#import <MGAlertView/MGAlertView.h>

@interface MGAlertView (ViewsUpdate)

- (void)updateTitleLabelAppearance;
- (void)updateMessageLabelAppearance;
- (void)updateButtonAppearance:(UIButton *)button isLastButton:(BOOL)lastButton;
@end
