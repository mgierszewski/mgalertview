//
//  UIImage+Blur.m
//
//  Created by Maciej Gierszewski on 24.09.2013.
//  Copyright (c) 2013 Maciej Gierszewski All rights reserved.
//

@import Accelerate;

#import "UIImage+MGAlertView.h"
#import "MGAlertViewUtils.h"

@implementation UIImage (MGAlertView)

+ (UIImage *)imageWithFillColor:(UIColor *)color
{
    CGSize size = CGSizeMake(1.0f, 1.0f);

    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [color setFill];
    UIRectFill(CGRectMake(0.0f, 0.0f, 1.0f, 1.0f));
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(UIImage *)blurImageWithBoxSize:(int)boxSize
{
    //Get CGImage from UIImage
    CGImageRef cgImage = self.CGImage;
    CGBitmapInfo info = CGImageGetBitmapInfo(cgImage);
    
    //setup variables
    vImage_Buffer inBuffer, outBuffer;
    vImage_Error error;
    
    void *pixelBuffer;
    
    //These two lines get get the data from the CGImage
    CGDataProviderRef inProvider = CGImageGetDataProvider(cgImage);
    CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
    
    //The next three lines set up the inBuffer object based on the attributes of the CGImage
    inBuffer.width = CGImageGetWidth(cgImage);
    inBuffer.height = CGImageGetHeight(cgImage);
    inBuffer.rowBytes = CGImageGetBytesPerRow(cgImage);
    inBuffer.data = (void *)CFDataGetBytePtr(inBitmapData);
    
    //create vImage_Buffer for output
    
    //allocate a buffer for the output image and check if it exists in the next three lines
    pixelBuffer = malloc(CGImageGetBytesPerRow(cgImage) * CGImageGetHeight(cgImage));
    if(pixelBuffer == 0)
    {
        DebugLog(@"No pixelbuffer");
    }
    
    //set up the output buffer object based on the same dimensions as the input image
    outBuffer.width = CGImageGetWidth(cgImage);
    outBuffer.height = CGImageGetHeight(cgImage);
    outBuffer.rowBytes = CGImageGetBytesPerRow(cgImage);
    outBuffer.data = pixelBuffer;
    
    //perform convolution - this is the call for our type of data
    error = vImageTentConvolve_ARGB8888(&inBuffer, &outBuffer, NULL, 0, 0, boxSize, boxSize, NULL, kvImageEdgeExtend);
    
    //check for an error in the call to perform the convolution
    if (error)
    {
        DebugLog(@"error from convolution %ld", error);
    }
    
    //create CGImageRef from vImage_Buffer output
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(outBuffer.data,
                                                 outBuffer.width,
                                                 outBuffer.height,
                                                 CGImageGetBitsPerComponent(cgImage),
                                                 outBuffer.rowBytes,
                                                 colorSpace,
                                                 info);
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    
    //clean up
    CGImageRelease(imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    free(pixelBuffer);
    CFRelease(inBitmapData);

    return returnImage;
}

@end
