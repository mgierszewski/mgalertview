//
//  MGAlertViewController.h
//
//  Created by Maciej Gierszewski on 19.02.2014.
//  Copyright (c) 2014 Maciej Gierszewski All rights reserved.
//

#import "MGAlertView.h"

@interface MGAlertViewController : UIViewController
@property (nonatomic, weak) UIWindow *parentWindow;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic) int blurBoxSize;
@property (nonatomic) MGAlertViewBackgroundBlurType backgroundBlurType;
@property (nonatomic, readonly) NSArray *visibleAlerts;

- (void)addAlertView:(MGAlertView *)alertView;
- (void)prepareToRemoveAlertView:(MGAlertView *)alertView;
- (void)removeAlertView:(MGAlertView *)alertView;


@end
