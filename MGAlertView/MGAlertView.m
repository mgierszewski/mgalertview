//
//  MGAlertView.m
//
//  Created by Maciej Gierszewski on 05.03.2013.
//  Copyright (c) 2013 Maciej Gierszewski All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MGAlertView.h"
#import "MGAlertViewContainerWindow.h"
#import "NSString+MGAlertView.h"
#import "UIImage+MGAlertView.h"
#import "UIView+MGAlertView.h"
#import "MGAlertView+Animation.h"
#import "MGAlertView+ViewsUpdate.h"

#pragma mark - Private Properties

@interface MGAlertView ()
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UIView *contentSeparatorView;
@property (nonatomic, strong) UIScrollView *customViewContainer;
@property (nonatomic, strong) UIScrollView *messageLabelContainer;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) NSArray *buttons;
@property (nonatomic, strong) NSArray *buttonSeparatorViews;

- (IBAction)buttonTouchedUp:(UIButton *)sender;
@end

@implementation MGAlertView

#pragma mark - Constructors

- (MGAlertView *)initWithMessage:(NSString *)message
						   image:(UIImage *)image
						delegate:(id<MGAlertViewDelegate>)delegate
						 buttons:(NSArray *)buttonTitles
{
	return [self initWithTitle:nil message:message image:image delegate:delegate buttons:buttonTitles];
}

- (MGAlertView *)initWithMessage:(NSString *)message
					  customView:(UIView *)view
						delegate:(id<MGAlertViewDelegate>)delegate
						 buttons:(NSArray *)buttonTitles
{
	return [self initWithTitle:nil message:message customView:view delegate:delegate buttons:buttonTitles];
}

- (MGAlertView *)initWithTitle:(NSString *)title
					   message:(NSString *)message
						 image:(UIImage *)image
					  delegate:(id<MGAlertViewDelegate>)delegate
					   buttons:(NSArray *)buttonTitles
{
	UIImageView *imageView = nil;
	if (image)
	{
		imageView = [[UIImageView alloc] initWithImage:image];
		imageView.contentMode = UIViewContentModeCenter;
	}
	return [self initWithTitle:title message:message customView:imageView delegate:delegate buttons:buttonTitles];
}

- (MGAlertView *)initWithTitle:(NSString *)title
					   message:(NSString *)message
					customView:(UIView *)customView
					  delegate:(id<MGAlertViewDelegate>)delegate
					   buttons:(NSArray *)buttonTitles
{
	self = [super init];
	if (self)
	{
		// --------
		// Defaults
		// --------
		_alertMargins =                     UIEdgeInsetsMake(20.0f, 10.0f, 10.0f, 10.0f);
		
		_alertBackgroundColor =             [UIColor colorWithWhite:0.0 alpha:0.9];
		_alertBackgroundImageAlpha =        0.5f;
		
		_buttonTitleColor =                 [UIColor colorWithWhite:1.0 alpha:1.0];
		_buttonHighlightedTitleColor =      [UIColor colorWithWhite:1.0 alpha:1.0];
		_buttonBackgroundColor =            [UIColor colorWithWhite:0.0 alpha:0.0];
		_buttonHighlightedBackgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
		_buttonFont =                       [UIFont systemFontOfSize:17.0f];
		
		_confirmationButtonBackgroundColor =                nil;
		_confirmationButtonTitleColor =                     nil;
		_confirmationButtonHighlightedBackgroundColor =     nil;
		_confirmationButtonHighlightedTitleColor =          nil;
		_confirmationButtonBorderColor =    nil;
		_confirmationButtonFont =           [UIFont boldSystemFontOfSize:17.0f];
		
		_buttonSeparatorColor =             [UIColor colorWithWhite:0.3 alpha:0.9];
		_buttonBorderColor =                [UIColor colorWithWhite:0.0 alpha:0.0];
		_messageTextColor =                 [UIColor colorWithWhite:0.8 alpha:1.0];
		_titleTextColor =                   [UIColor colorWithWhite:0.8 alpha:1.0];
		_messageFont =                      [UIFont systemFontOfSize:15.0f];
		_titleFont =                        [UIFont boldSystemFontOfSize:17.0f];
		
		_contentButtonSeparatorColor =		nil;
		_contentMargins =                   UIEdgeInsetsMake(15.0f, 15.0f, 15.0f, 15.0f);
		_buttonGroupMargins =               UIEdgeInsetsZero;
		_buttonContentInsets =              UIEdgeInsetsMake(12.0f, 8.0f, 11.0f, 8.0f);
		_buttonCornerRadius =               0.0f;
		_buttonBorderWidth =                0.0f;
		_buttonHorizontalSpacing =          0.0f;
		_buttonVerticalSpacing =            0.0f;
		_cornerRadius =                     6.0f;
		_contentVerticalSpacing =           8.0f;
		_contentHorizontalSpacing =         8.0f;
		
		_lastButtonIsConfirmationButton =   [buttonTitles count] > 1;
		
		_animationType = MGAlertViewAnimationTypeZoomOut;
		
		// --
		_delegate = delegate;
		
		self.clipsToBounds = YES;
		self.layer.cornerRadius = self.cornerRadius;
		self.backgroundColor = self.alertBackgroundColor;
		self.autoresizingMask = UIViewAutoresizingNone;
		
		self.backgroundImageView = [UIImageView new];
		self.backgroundImageView.clipsToBounds = YES;
		self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
		self.backgroundImageView.alpha = 0.0f;
		[self addSubview:self.backgroundImageView];
		
		// motion effect
		if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f)
		{
			CGFloat offset = 20.0f;
			
			UIInterpolatingMotionEffect *verticalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
			verticalMotionEffect.minimumRelativeValue = @(-offset);
			verticalMotionEffect.maximumRelativeValue = @(offset);
			
			UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
			horizontalMotionEffect.minimumRelativeValue = @(-offset);
			horizontalMotionEffect.maximumRelativeValue = @(offset);
			
			UIMotionEffectGroup *group = [UIMotionEffectGroup new];
			group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
			
			[self addMotionEffect:group];
		}
		
		// -----------------
		// Creating subviews
		// -----------------
		if (customView)
		{
			self.customView = customView;
			
			self.customViewContainer = [[UIScrollView alloc] initWithFrame:CGRectZero];
			self.customViewContainer.scrollEnabled = NO;
			self.customViewContainer.backgroundColor = [UIColor clearColor];
			self.customViewContainer.clipsToBounds = YES;
			self.customViewContainer.autoresizingMask = UIViewAutoresizingNone;
			self.customViewContainer.showsHorizontalScrollIndicator = NO;
			self.customViewContainer.showsVerticalScrollIndicator = NO;
			
			[self.customViewContainer addSubview:self.customView];
			[self addSubview:self.customViewContainer];
		}
		if (title && [title isKindOfClass:[NSString class]] && [title length] > 0)
		{
			self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
			self.titleLabel.backgroundColor = [UIColor clearColor];
			self.titleLabel.textAlignment = NSTextAlignmentCenter;
			self.titleLabel.numberOfLines = 0;
			self.titleLabel.autoresizingMask = UIViewAutoresizingNone;
			self.titleLabel.text = title;
			self.titleLabel.clipsToBounds = YES;
			
			[self addSubview:self.titleLabel];
		}
		if (message && [message isKindOfClass:[NSString class]] && [message length] > 0)
		{
			self.messageLabelContainer = [[UIScrollView alloc] initWithFrame:CGRectZero];
			self.messageLabelContainer.scrollEnabled = NO;
			self.messageLabelContainer.backgroundColor = [UIColor clearColor];
			self.messageLabelContainer.clipsToBounds = YES;
			self.messageLabelContainer.autoresizingMask = UIViewAutoresizingNone;
			self.messageLabelContainer.showsHorizontalScrollIndicator = NO;
			self.messageLabelContainer.showsVerticalScrollIndicator = NO;
			
			self.messageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
			self.messageLabel.backgroundColor = [UIColor clearColor];
			self.messageLabel.textAlignment = NSTextAlignmentCenter;
			self.messageLabel.numberOfLines = 0;
			self.messageLabel.autoresizingMask = UIViewAutoresizingNone;
			self.messageLabel.text = message;
			
			[self.messageLabelContainer addSubview:self.messageLabel];
			[self addSubview:self.messageLabelContainer];
		}
		
		// --------------
		// Create buttons
		// --------------
		if ([buttonTitles count] > 0)
		{
			// buttons
			NSMutableArray *mutableButtons = [NSMutableArray array];
			[buttonTitles enumerateObjectsUsingBlock:^(NSString *buttonTitle, NSUInteger idx, BOOL *stop) {
				
				UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
				[button setAutoresizingMask:UIViewAutoresizingNone];
				[button setTitle:buttonTitle forState:UIControlStateNormal];
				[button addTarget:self action:@selector(buttonTouchedUp:) forControlEvents:UIControlEventTouchUpInside];
				
				[self addSubview:button];
				[mutableButtons addObject:button];
			}];
			self.buttons = [NSArray arrayWithArray:mutableButtons];
			
			// button separators
			NSMutableArray *mutableButtonSeparatorViews = [NSMutableArray array];
			for (int i = 0; i < [buttonTitles count] - 1; i++)
			{
				UIView *buttonSeparatorView = [UIView new];
				buttonSeparatorView.autoresizingMask = UIViewAutoresizingNone;
				buttonSeparatorView.backgroundColor = self.buttonSeparatorColor;
				
				[self addSubview:buttonSeparatorView];
				[mutableButtonSeparatorViews addObject:buttonSeparatorView];
			}
			self.buttonSeparatorViews = [NSArray arrayWithArray:mutableButtonSeparatorViews];
			
			// content separator
			self.contentSeparatorView = [UIView new];
			self.contentSeparatorView.autoresizingMask = UIViewAutoresizingNone;
			self.contentSeparatorView.backgroundColor = self.buttonSeparatorColor;
			[self addSubview:self.contentSeparatorView];
		}
		
		// ----------------
		// update apperance
		// ----------------
		[self updateTitleLabelAppearance];
		[self updateMessageLabelAppearance];
		[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
			[self updateButtonAppearance:button isLastButton:idx == [self.buttons count] - 1];
		}];
	}
	return self;
}

#pragma mark - Layout

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	// ---------------------
	// Maximum possible size
	// ---------------------
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	CGRect superviewFrame = CGRectApplyAffineTransform(self.superview.frame, self.superview.transform);
	superviewFrame.origin = CGPointZero;
	
	UIEdgeInsets alertMargins = self.alertMargins;
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && CGRectGetWidth(superviewFrame) < CGRectGetWidth(screenBounds))
	{
		// detect displayng as second app on the ipad
		alertMargins = UIEdgeInsetsMake(20.0f, 10.0f, 10.0f, 10.0f);
	}
	CGRect windowFrame = UIEdgeInsetsInsetRect(superviewFrame, alertMargins);
	CGSize maximumSize = windowFrame.size;
	
	// -----------------------
	// Compute the alerts size
	// -----------------------
	BOOL alignButtonsVertically = NO;
	
	CGSize contentSize = CGSizeZero;
	CGSize singleButtonSize = CGSizeZero;
	CGSize buttonsGroupSize = CGSizeZero;
	
	CGRect titleFrame = CGRectZero;
	CGRect messageFrame = CGRectZero;
	CGRect messageContainerFrame = CGRectZero;
	CGRect customViewFrame = CGRectZero;
	CGRect customViewContainerFrame = CGRectZero;
	
	// title label
	NSInteger contentViews = 0;
	if (self.titleLabel)
	{
		contentViews++;
		
		CGFloat maximumWidth = maximumSize.width - self.contentMargins.left - self.contentMargins.right;
		titleFrame.size = [self.titleLabel.text sizeWithFont:self.titleFont maximumWidth:maximumWidth];
		
		contentSize.width = MAX(contentSize.width, CGRectGetWidth(titleFrame));
		contentSize.height += CGRectGetHeight(titleFrame);
	}
	
	// message label
	if (self.messageLabel)
	{
		contentViews++;
		
		CGFloat maximumWidth = maximumSize.width - self.contentMargins.left - self.contentMargins.right;
		messageFrame.size = [self.messageLabel.text sizeWithFont:self.messageFont maximumWidth:maximumWidth];
		messageContainerFrame.size = messageFrame.size;
		
		contentSize.width = MAX(contentSize.width, CGRectGetWidth(messageContainerFrame));
		contentSize.height += CGRectGetHeight(messageContainerFrame);
	}
	
	// custom view
	if (self.customView)
	{
		contentViews++;
		
		CGFloat maximumWidth = maximumSize.width - self.contentMargins.left - self.contentMargins.right;
		
		customViewFrame = self.customView.frame;
		customViewFrame.origin = CGPointZero;
		
		customViewContainerFrame.size = CGSizeMake(MIN(ceilf(CGRectGetWidth(customViewFrame)), maximumWidth), ceilf(CGRectGetHeight(customViewFrame)));
		
		contentSize.width = MAX(contentSize.width, CGRectGetWidth(customViewContainerFrame));
		contentSize.height += CGRectGetHeight(customViewContainerFrame);
	}
	
	// content vertical spacing
	contentSize.height += MAX(0, contentViews - 1) * self.contentVerticalSpacing;
	
	// -------
	// Buttons
	// -------
	if ([self.buttons count] > 0)
	{
		// computing widest buttons width
		CGFloat widestButtonWidth = 0;
		CGFloat buttonHeight = 0;
		
		for (UIButton *button in self.buttons)
		{
			CGSize buttonSize = [[button titleForState:UIControlStateNormal] sizeWithFont:button.titleLabel.font maximumWidth:0.0f];
			UIEdgeInsets buttonContentInsets = button.contentEdgeInsets;
			
			widestButtonWidth = MAX(widestButtonWidth, ceilf(buttonSize.width) + buttonContentInsets.left + buttonContentInsets.right);
			buttonHeight = MAX(buttonHeight, ceilf(buttonSize.height) + buttonContentInsets.top + buttonContentInsets.bottom);
		}
		
		// check if buttons can be aligned vertically
		CGFloat maximumWidth = maximumSize.width - self.buttonGroupMargins.left - self.buttonGroupMargins.right;
		CGFloat minimumWidth = contentSize.width + self.contentMargins.left + self.contentMargins.right - self.buttonGroupMargins.left - self.buttonGroupMargins.right;
		
		CGFloat buttonSeparatorsWidth = self.buttonHorizontalSpacing * ([self.buttons count] - 1);
		CGFloat buttonSeparatorsHeight = self.buttonVerticalSpacing * ([self.buttons count] - 1);
		
		// check if buttons fit
		if (widestButtonWidth * [self.buttons count] + buttonSeparatorsWidth > maximumWidth)
		{
			// they won't fit horizontally -> vertical alignment
			alignButtonsVertically = YES;
			
			singleButtonSize = CGSizeMake(MAX(minimumWidth, MIN(maximumWidth, widestButtonWidth)), buttonHeight);
			buttonsGroupSize = CGSizeMake(singleButtonSize.width, [self.buttons count] * singleButtonSize.height + buttonSeparatorsHeight);
		}
		else if (widestButtonWidth * [self.buttons count] + buttonSeparatorsWidth < minimumWidth)
		{
			// horizontal alignment -> make buttons wider
			singleButtonSize = CGSizeMake(ceilf((minimumWidth - buttonSeparatorsWidth) / [self.buttons count]), buttonHeight);
			buttonsGroupSize = CGSizeMake(singleButtonSize.width * [self.buttons count] + buttonSeparatorsWidth, buttonHeight);
		}
		else
		{
			// horizontal alignment
			singleButtonSize = CGSizeMake(widestButtonWidth, buttonHeight);
			buttonsGroupSize = CGSizeMake(singleButtonSize.width * [self.buttons count] + buttonSeparatorsWidth, buttonHeight);
		}
	}
	
	CGSize buttonsGroupWithMarginsSize = [self.buttons count] > 0 ? CGSizeMake(self.buttonGroupMargins.left + buttonsGroupSize.width + self.buttonGroupMargins.right, self.buttonGroupMargins.top + buttonsGroupSize.height + self.buttonGroupMargins.bottom) : CGSizeZero;
	CGSize contentWithMarginsSize = CGSizeMake(self.contentMargins.left + contentSize.width + self.contentMargins.right, self.contentMargins.top + contentSize.height + self.contentMargins.bottom);
 
	// ---------------------------
	// fitting to the maximum size
	// ---------------------------
	if (buttonsGroupWithMarginsSize.height + contentWithMarginsSize.height > maximumSize.height)
	{
		contentWithMarginsSize.height = MAX(0.0f, maximumSize.height - buttonsGroupWithMarginsSize.height);
		contentSize.height = MAX(0.0f, contentWithMarginsSize.height - self.contentMargins.top - self.contentMargins.bottom);
		
		CGFloat titleSeparator = (CGRectGetHeight(titleFrame) > 0 ? self.contentVerticalSpacing : 0.0f);
		CGFloat customViewSeparator = (CGRectGetHeight(customViewFrame) > 0 ? self.contentVerticalSpacing : 0.0f);
		
		// crop the content
		if (CGRectGetHeight(titleFrame) > contentSize.height)
		{
			// title doesn't fit
			titleFrame.size.height = contentSize.height;
			customViewContainerFrame.size.height = 0.0f;
			messageContainerFrame.size.height = 0.0f;
		}
		else if (CGRectGetHeight(titleFrame) + titleSeparator + CGRectGetHeight(customViewContainerFrame) > contentSize.height)
		{
			// custom view doesn't fit
			customViewContainerFrame.size.height = contentSize.height - titleSeparator - CGRectGetHeight(titleFrame);
			messageContainerFrame.size.height = 0.0f;
		}
		else if (CGRectGetHeight(titleFrame) + titleSeparator + CGRectGetHeight(customViewFrame) + customViewSeparator + CGRectGetHeight(messageContainerFrame) > contentSize.height)
		{
			// message doesn't fit
			messageContainerFrame.size.height = contentSize.height - customViewSeparator - CGRectGetHeight(customViewFrame) - titleSeparator - CGRectGetHeight(titleFrame);
		}
	}
	
	// ------
	// layout
	// ------
	CGSize alertSize = CGSizeMake(MAX(contentWithMarginsSize.width, buttonsGroupWithMarginsSize.width), contentWithMarginsSize.height + buttonsGroupWithMarginsSize.height);
	
	CGRect frame = CGRectZero;
	frame.origin.x = lrint(CGRectGetMidX(windowFrame) - alertSize.width/2.0f);
	frame.origin.y = lrint(CGRectGetMidY(windowFrame) - alertSize.height/2.0f);
	frame.size = alertSize;
	
	// layout subviews
	CGFloat yPosition = self.contentMargins.top;
	if (self.titleLabel)
	{
		titleFrame.origin.x = lrint(alertSize.width/2.0f - CGRectGetWidth(titleFrame)/2.0f);
		titleFrame.origin.y = lrint(yPosition);
		
		self.titleLabel.frame = titleFrame;
		yPosition = CGRectGetMaxY(titleFrame);
	}
	if (self.messageLabel)
	{
		if (self.titleLabel)
		{
			yPosition += self.contentVerticalSpacing;
		}
		messageContainerFrame.origin.x = lrint(alertSize.width/2.0f - CGRectGetWidth(messageContainerFrame)/2.0f);
		messageContainerFrame.origin.y = lrint(yPosition);
		
		self.messageLabelContainer.frame = messageContainerFrame;
		self.messageLabelContainer.contentSize = messageFrame.size;
		self.messageLabelContainer.scrollEnabled = (CGRectGetHeight(messageContainerFrame) < CGRectGetHeight(messageFrame));
		
		self.messageLabel.frame = messageFrame;
		yPosition = CGRectGetMaxY(messageContainerFrame);
	}
	if (self.customView)
	{
		if (self.titleLabel || self.messageLabel)
		{
			yPosition += self.contentVerticalSpacing;
		}
		customViewContainerFrame.origin.y = lrint(yPosition);
		customViewContainerFrame.origin.x = lrint(alertSize.width/2.0f - CGRectGetWidth(customViewContainerFrame)/2.0f);
		
		self.customViewContainer.frame = customViewContainerFrame;
		self.customViewContainer.contentSize = customViewFrame.size;
		self.customViewContainer.scrollEnabled = (CGRectGetHeight(customViewContainerFrame) < CGRectGetHeight(customViewFrame) || CGRectGetWidth(customViewContainerFrame) < CGRectGetHeight(customViewFrame));
	}
	
	// ----------
	// background
	// ----------
	self.backgroundImageView.frame = (CGRect){CGPointZero, alertSize};
	
	// -------
	// buttons
	// -------
	if ([self.buttons count] > 0)
	{
		yPosition = alertSize.height - buttonsGroupWithMarginsSize.height;
		
		// separators
		CGFloat lineWidth = 1.0f / [[UIScreen mainScreen] scale];
		
		CGRect contentSeparatorFrame = self.contentSeparatorView.frame;
		contentSeparatorFrame.origin.x = 0.0f;
		contentSeparatorFrame.origin.y = yPosition;
		contentSeparatorFrame.size.width = alertSize.width;
		contentSeparatorFrame.size.height = lineWidth;
		self.contentSeparatorView.frame = contentSeparatorFrame;
		
		[self.buttonSeparatorViews enumerateObjectsUsingBlock:^(UIView *separatorView, NSUInteger idx, BOOL *stop) {
			CGRect separatorFrame = separatorView.frame;
			if (alignButtonsVertically)
			{
				separatorFrame.origin.x = 0.0f;
				separatorFrame.origin.y = yPosition + (idx + 1) * (singleButtonSize.height + self.buttonVerticalSpacing) + self.buttonVerticalSpacing/2.0f;
				separatorFrame.size.width = alertSize.width;
				separatorFrame.size.height = lineWidth;
			}
			else
			{
				separatorFrame.origin.x = (idx + 1) * (singleButtonSize.width + self.buttonHorizontalSpacing) + self.buttonHorizontalSpacing/2.0f;
				separatorFrame.origin.y = yPosition + lineWidth + self.buttonGroupMargins.top;
				separatorFrame.size.width = lineWidth;
				separatorFrame.size.height = buttonsGroupSize.height - lineWidth;
			}
			
			if ([[UIView class] respondsToSelector:@selector(performWithoutAnimation:)])
			{
				[UIView performWithoutAnimation:^{
					separatorView.frame = separatorFrame;
				}];
			}
			else
			{
				separatorView.frame = separatorFrame;
			}
		}];
		
		// buttons
		[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
			if (alignButtonsVertically)
			{
				button.frame = CGRectMake(self.buttonGroupMargins.left, self.buttonGroupMargins.top + yPosition + idx * (singleButtonSize.height + self.buttonVerticalSpacing), singleButtonSize.width, singleButtonSize.height);
			}
			else
			{
				CGFloat buttonWidth = singleButtonSize.width;
				if (idx == [self.buttons count] - 1)
				{
					// last button fills rest of the remaining space
					buttonWidth = alertSize.width - (self.buttonGroupMargins.left + idx * (singleButtonSize.width + self.buttonHorizontalSpacing) + self.buttonGroupMargins.right);
				}
				button.frame = CGRectMake(self.buttonGroupMargins.left + idx * (singleButtonSize.width + self.buttonHorizontalSpacing), self.buttonGroupMargins.top + yPosition, buttonWidth, singleButtonSize.height);
			}
		}];
	}
	
	// adjust center if keyboard is visible
	CGFloat keyboardHeight = [[[[MGAlertViewContainerWindow sharedInstance] alertViewController] valueForKey:@"keyboardHeight"] floatValue];
	if (keyboardHeight > 0.0f)
	{
		UIView *visibleControl = [self findActiveTextInput] ?: self.buttons.lastObject;
		CGRect visibleControlFrame = visibleControl.frame;
		UIView *superview = visibleControl.superview;
		
		// compute active control's frame in window's coordinate space
		while (superview && superview != self.window)
		{
			// use target frame instead of actual frame if superview is self
			CGRect superviewFrame = superview == self ? frame : superview.frame;
			
			// --
			visibleControlFrame = CGRectOffset(visibleControlFrame, CGRectGetMinX(superviewFrame), CGRectGetMinY(superviewFrame));
			superview = superview.superview;
		}
		
		// window's frame including keyboard
		CGRect windowWithKeyboardFrame = UIEdgeInsetsInsetRect(windowFrame, UIEdgeInsetsMake(0.0f, 0.0f, keyboardHeight, 0.0f));
		
		// min and max Y position of text input
		CGFloat minY = CGRectGetMinY(windowWithKeyboardFrame);
		CGFloat maxY = CGRectGetMaxY(windowWithKeyboardFrame);
		
		// space between bottom of the alert and bottom of the window
		// = how much alert should be moved up not to hide bottom button
		CGFloat offset = MIN(0.0f, CGRectGetMaxY(windowWithKeyboardFrame) - CGRectGetMaxY(frame));
		
		// adjust the offset so that active control would be still visible
		if (CGRectGetMinY(visibleControlFrame) + offset < minY)
		{
			offset = minY - CGRectGetMinY(visibleControlFrame);
		}
		else if (CGRectGetMaxY(visibleControlFrame) + offset > maxY)
		{
			offset = maxY - CGRectGetMaxY(visibleControlFrame);
		}
		
		self.frame = CGRectOffset(frame, 0.0f, offset);
	}
	else
	{
		self.frame = frame;
	}
}

#pragma mark - Show / Hide

- (void)show:(BOOL)animated
{
	[[[MGAlertViewContainerWindow sharedInstance] alertViewController] addAlertView:self];
	
	if (animated)
	{
		[self showAnimation];
	}
}

- (void)hide:(BOOL)animated
{
	[[[MGAlertViewContainerWindow sharedInstance] alertViewController] prepareToRemoveAlertView:self];
	if (animated)
	{
		[self hideAnimation];
	}
	else
	{
		[[[MGAlertViewContainerWindow sharedInstance] alertViewController] removeAlertView:self];
	}
}

- (void)hide:(BOOL)animated afterDelay:(NSTimeInterval)delay
{
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		
		[self hide:animated];
	});
}

#pragma mark - Internal Setters

- (void)setCustomView:(UIView *)customView
{
	// check if custom view contains text field
	customView.autoresizingMask = UIViewAutoresizingNone;
	
	[customView findViewConformingProtocol:@protocol(UITextInput) foundBlock:^(id view) {
		[(UIResponder<UITextInput>*)view becomeFirstResponder];
	}];
	
	[customView findViewOfClass:[UIActivityIndicatorView class] foundBlock:^(UIView *view) {
		UIActivityIndicatorView *loader = (UIActivityIndicatorView *)view;
		
		[loader setFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f)];
		[loader startAnimating];
		[loader setColor:self.messageTextColor];
	}];
	
	_customView = customView;
}

#pragma mark - Appearance setters

- (void)setCornerRadius:(CGFloat)cornerRadius
{
	_cornerRadius = cornerRadius;
	self.layer.cornerRadius = cornerRadius;
}

- (void)setAlertMargins:(UIEdgeInsets)alertMargins
{
	_alertMargins = alertMargins;
	[self setNeedsLayout];
}

- (void)setAlertBackgroundColor:(UIColor *)alertBackgroundColor
{
	_alertBackgroundColor = alertBackgroundColor;
	self.backgroundColor = alertBackgroundColor;
}

- (void)setAlertBackgroundImage:(UIImage *)alertBackgroundImage
{
	// set the image in the container
	if (!self.backgroundImageView.image && alertBackgroundImage)
	{
		self.backgroundImageView.image = alertBackgroundImage;
		
		[UIView animateWithDuration:MGAlertViewShowAnimationDuration / 2.0
						 animations:^{
							 self.backgroundImageView.alpha = self.alertBackgroundImageAlpha;
							 self.backgroundColor = [self.alertBackgroundColor colorWithAlphaComponent:1.0f];
						 }
						 completion:^(BOOL finished) {
						 }];
	}
	else if (self.backgroundImageView.image && !alertBackgroundImage)
	{
		[UIView animateWithDuration:MGAlertViewShowAnimationDuration / 2.0
						 animations:^{
							 self.backgroundImageView.alpha = 0.0f;
							 self.backgroundColor = self.alertBackgroundColor;
						 }
						 completion:^(BOOL finished) {
							 self.backgroundImageView.image = alertBackgroundImage;
						 }];
	}
	else
	{
		self.backgroundImageView.image = alertBackgroundImage;
	}
}

- (void)setAlertBackgroundImageAlpha:(CGFloat)alertBackgroundImageAlpha
{
	_alertBackgroundImageAlpha = alertBackgroundImageAlpha;
	if (self.backgroundImageView.image)
	{
		[UIView animateWithDuration:0.0
							  delay:0.0
							options:UIViewAnimationOptionBeginFromCurrentState
						 animations:^{
							 self.backgroundImageView.alpha = alertBackgroundImageAlpha;
						 }
						 completion:^(BOOL finished) {
						 }];
	}
}

#pragma mark buttons

- (void)setButtonFont:(UIFont *)buttonFont
{
	if (!buttonFont)
	{
		return;
	}
	_buttonFont = buttonFont;
	
	[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
		[self updateButtonAppearance:button isLastButton:([self.buttons lastObject] == button)];
	}];
}

- (void)setButtonTitleColor:(UIColor *)buttonTitleColor
{
	_buttonTitleColor = buttonTitleColor;
	
	[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
		[self updateButtonAppearance:button isLastButton:([self.buttons lastObject] == button)];
	}];
}

- (void)setButtonHighlightedTitleColor:(UIColor *)buttonHighlightedTitleColor
{
	_buttonHighlightedTitleColor = buttonHighlightedTitleColor;
	
	[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
		[self updateButtonAppearance:button isLastButton:idx == [self.buttons count] - 1];
	}];
}

- (void)setButtonBackgroundColor:(UIColor *)buttonBackgroundColor
{
	_buttonBackgroundColor = buttonBackgroundColor;
	
	[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
		[self updateButtonAppearance:button isLastButton:idx == [self.buttons count] - 1];
	}];
}

- (void)setButtonHighlightedBackgroundColor:(UIColor *)buttonHighlightedBackgroundColor
{
	_buttonHighlightedBackgroundColor = buttonHighlightedBackgroundColor;
	
	[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
		[self updateButtonAppearance:button isLastButton:idx == [self.buttons count] - 1];
	}];
}

- (void)setButtonBorderColor:(UIColor *)buttonBorderColor
{
	_buttonBorderColor = buttonBorderColor;
	
	[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
		[self updateButtonAppearance:button isLastButton:idx == [self.buttons count] - 1];
	}];
}

#pragma mark buttons (layout)

- (void)setButtonGroupMargins:(UIEdgeInsets)buttonGroupMargins
{
	_buttonGroupMargins = buttonGroupMargins;
	[self setNeedsLayout];
}

- (void)setButtonContentInsets:(UIEdgeInsets)buttonContentInsets
{
	_buttonContentInsets = buttonContentInsets;
	
	[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
		[self updateButtonAppearance:button isLastButton:idx == [self.buttons count] - 1];
	}];
}

- (void)setButtonCornerRadius:(CGFloat)buttonCornerRadius
{
	_buttonCornerRadius = buttonCornerRadius;
	
	[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
		[self updateButtonAppearance:button isLastButton:idx == [self.buttons count] - 1];
	}];
}

- (void)setButtonBorderWidth:(CGFloat)buttonBorderWidth
{
	_buttonBorderWidth = buttonBorderWidth;
	
	[self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
		[self updateButtonAppearance:button isLastButton:idx == [self.buttons count] - 1];
	}];
}

- (void)setButtonHorizontalSpacing:(CGFloat)buttonHorizontalSpacing
{
	_buttonHorizontalSpacing = buttonHorizontalSpacing;
	[self setNeedsLayout];
}

- (void)setButtonVerticalSpacing:(CGFloat)buttonVerticalSpacing
{
	_buttonVerticalSpacing = buttonVerticalSpacing;
	[self setNeedsLayout];
}

- (void)setButtonSeparatorColor:(UIColor *)buttonSeparatorColor
{
	_buttonSeparatorColor = buttonSeparatorColor;
	
	self.contentSeparatorView.backgroundColor = self.contentButtonSeparatorColor ?: buttonSeparatorColor;
	[self.buttonSeparatorViews enumerateObjectsUsingBlock:^(UIView *view, NSUInteger idx, BOOL *stop) {
		view.backgroundColor = buttonSeparatorColor;
	}];
}

#pragma mark confirmation button

- (void)setConfirmationButtonFont:(UIFont *)confirmationButtonFont
{
	if (!confirmationButtonFont)
	{
		return;
	}
	_confirmationButtonFont = confirmationButtonFont;
	
	[self updateButtonAppearance:[self.buttons lastObject] isLastButton:YES];
}

- (void)setConfirmationButtonTitleColor:(UIColor *)confirmationButtonTitleColor
{
	_confirmationButtonTitleColor = confirmationButtonTitleColor;
	
	[self updateButtonAppearance:[self.buttons lastObject] isLastButton:YES];
}

- (void)setConfirmationButtonHighlightedTitleColor:(UIColor *)confirmationButtonHighlightedTitleColor
{
	_confirmationButtonHighlightedTitleColor = confirmationButtonHighlightedTitleColor;
	
	[self updateButtonAppearance:[self.buttons lastObject] isLastButton:YES];
}

- (void)setConfirmationButtonBackgroundColor:(UIColor *)confirmationButtonBackgroundColor
{
	_confirmationButtonBackgroundColor = confirmationButtonBackgroundColor;
	
	[self updateButtonAppearance:[self.buttons lastObject] isLastButton:YES];
}

- (void)setConfirmationButtonHighlightedBackgroundColor:(UIColor *)confirmationButtonHighlightedBackgroundColor
{
	_confirmationButtonHighlightedBackgroundColor = confirmationButtonHighlightedBackgroundColor;
	
	[self updateButtonAppearance:[self.buttons lastObject] isLastButton:YES];
}

- (void)setConfirmationButtonBorderColor:(UIColor *)confirmationButtonBorderColor
{
	_confirmationButtonBorderColor = confirmationButtonBorderColor;
	
	[self updateButtonAppearance:[self.buttons lastObject] isLastButton:YES];
}

- (void)setLastButtonIsConfirmationButton:(NSInteger)lastButtonIsConfirmationButton
{
	_lastButtonIsConfirmationButton = lastButtonIsConfirmationButton;
	
	[self updateButtonAppearance:[self.buttons lastObject] isLastButton:YES];
}

#pragma mark title

- (void)setTitleTextColor:(UIColor *)titleTextColor
{
	_titleTextColor = titleTextColor;
	[self updateTitleLabelAppearance];
}

- (void)setTitleFont:(UIFont *)titleFont
{
	if (!titleFont)
	{
		return;
	}
	_titleFont = titleFont;
	
	[self updateTitleLabelAppearance];
	[self setNeedsLayout];
}

#pragma mark message

- (void)setMessageTextColor:(UIColor *)messageTextColor
{
	_messageTextColor = messageTextColor;
	_messageLabel.textColor = messageTextColor;
	
	// change loader color
	[self.customView findViewOfClass:[UIActivityIndicatorView class] foundBlock:^(UIView *view) {
		UIActivityIndicatorView *loader = (UIActivityIndicatorView *)view;
		[loader setColor:self.messageTextColor];
	}];
}

- (void)setMessageFont:(UIFont *)messageFont
{
	if (!messageFont)
	{
		return;
	}
	_messageFont = messageFont;
	
	[self updateMessageLabelAppearance];
	[self setNeedsLayout];
}

#pragma mark content

- (void)setContentMargins:(UIEdgeInsets)contentMargins
{
	_contentMargins = contentMargins;
	[self setNeedsLayout];
}

- (void)setContentVerticalSpacing:(CGFloat)contentVerticalSpacing
{
	_contentVerticalSpacing = contentVerticalSpacing;
	[self setNeedsLayout];
}

- (void)setContentHorizontalSpacing:(CGFloat)contentHorizontalSpacing
{
	_contentHorizontalSpacing = contentHorizontalSpacing;
	[self setNeedsLayout];
}

- (void)setContentButtonSeparatorColor:(UIColor *)contentButtonSeparatorColor
{
	_contentButtonSeparatorColor = contentButtonSeparatorColor;
	self.contentSeparatorView.backgroundColor = contentButtonSeparatorColor;
}

#pragma mark animation type

- (void)setAnimationType:(MGAlertViewAnimationType)animationType
{
	_animationType = animationType;
}

#pragma mark - Button handling

- (IBAction)buttonTouchedUp:(UIButton *)sender
{
	NSInteger index = [self.buttons indexOfObject:sender];
	
	BOOL shouldHide = YES;
	if ([self.delegate respondsToSelector:@selector(alertView:shouldHideWithButtonIndex:)])
	{
		shouldHide = [self.delegate alertView:self shouldHideWithButtonIndex:index];
	}
	
	if (shouldHide)
	{
		if ([self.delegate respondsToSelector:@selector(alertView:clickedButtonAtIndex:)])
		{
			[self.delegate alertView:self clickedButtonAtIndex:index];
		}
		[self hide:YES];
	}
}

#pragma mark - Getters

- (UIImage *)alertBackgroundImage
{
	return self.backgroundImageView.image;
}

- (NSInteger)numberOfButtons
{
	return [self.buttons count];
}

- (NSString *)buttonTitleAtIndex:(NSInteger)index
{
	if (index < 0 || index >= [self.buttons count])
	{
		return nil;
	}
	return [self.buttons[index] titleForState:UIControlStateNormal];
}

@end