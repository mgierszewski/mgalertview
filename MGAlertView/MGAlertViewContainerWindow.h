//
//  AlertViewContainer.h
//
//  Created by Maciej Gierszewski on 24.09.2013.
//  Copyright (c) 2013 Maciej Gierszewski All rights reserved.
//

#import "MGAlertViewController.h"

@interface MGAlertViewContainerWindow : UIWindow
@property (nonatomic, strong) MGAlertViewController *alertViewController;

+ (MGAlertViewContainerWindow *)sharedInstance;
@end
