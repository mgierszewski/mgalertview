//
//  MGAlertView+ViewsUpdate.m
//  MGAlertView
//
//  Created by Maciek Gierszewski on 22.10.2015.
//  Copyright © 2015 Maciej Gierszewski. All rights reserved.
//

#import "MGAlertView+ViewsUpdate.h"
#import "UIImage+MGAlertView.h"

@implementation MGAlertView (ViewsUpdate)

- (void)updateButtonAppearance:(UIButton *)button isLastButton:(BOOL)lastButton
{
	button.layer.cornerRadius = self.buttonCornerRadius;
	button.layer.borderWidth = self.buttonBorderWidth;
	button.layer.borderColor = self.buttonBorderColor.CGColor;
	button.titleLabel.font = self.buttonFont;
	
	[button setBackgroundImage:[UIImage imageWithFillColor:self.buttonBackgroundColor] forState:UIControlStateNormal];
	[button setBackgroundImage:[UIImage imageWithFillColor:self.buttonHighlightedBackgroundColor] forState:UIControlStateHighlighted];
	[button setTitleColor:self.buttonTitleColor forState:UIControlStateNormal];
	[button setTitleColor:self.buttonHighlightedTitleColor forState:UIControlStateHighlighted];
	[button setContentEdgeInsets:self.buttonContentInsets];
	
	if (lastButton && (BOOL)self.lastButtonIsConfirmationButton)
	{
		if (self.confirmationButtonBorderColor)
		{
			button.layer.borderColor = self.confirmationButtonBorderColor.CGColor;
		}
		if (self.confirmationButtonFont)
		{
			button.titleLabel.font = self.confirmationButtonFont;
		}
		if (self.confirmationButtonBackgroundColor)
		{
			[button setBackgroundImage:[UIImage imageWithFillColor:self.confirmationButtonBackgroundColor] forState:UIControlStateNormal];
		}
		if (self.confirmationButtonTitleColor)
		{
			[button setTitleColor:self.confirmationButtonTitleColor forState:UIControlStateNormal];
		}
		if (self.confirmationButtonHighlightedBackgroundColor)
		{
			[button setBackgroundImage:[UIImage imageWithFillColor:self.confirmationButtonHighlightedBackgroundColor] forState:UIControlStateHighlighted];
		}
		if (self.confirmationButtonHighlightedTitleColor)
		{
			[button setTitleColor:self.confirmationButtonHighlightedTitleColor forState:UIControlStateHighlighted];
		}
	}
	[self setNeedsLayout];
}

- (void)updateTitleLabelAppearance
{
	self.titleLabel.textColor = self.titleTextColor;
	self.titleLabel.font = self.titleFont;
}

- (void)updateMessageLabelAppearance
{
	self.messageLabel.textColor = self.messageTextColor;
	self.messageLabel.font = self.messageFont;
}

@end
