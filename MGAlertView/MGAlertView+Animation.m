//
//  MGAlertView+Animation.m
//  MGAlertView
//
//  Created by Maciek Gierszewski on 22.10.2015.
//  Copyright © 2015 Maciej Gierszewski. All rights reserved.
//

#import "MGAlertView+Animation.h"
#import "MGAlertViewContainerWindow.h"

@implementation MGAlertView (Animation)

#pragma mark - Animation

- (void)showAnimation
{
	// -- alpha
	CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
	alphaAnimation.fromValue = @(0.0f);
	alphaAnimation.toValue = @(1.0f);
	alphaAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
	
	// --
	NSMutableArray *values = [NSMutableArray array];
	NSMutableArray *timingFunctions = [NSMutableArray array];
	
	if (self.animationType == MGAlertViewAnimationTypeBounce)
	{
		for (int i = -1; i < 11; i++) {
			
			CGFloat value = MAX(0.5f, 1.0f + sin(i * M_PI_2) * 0.25 * pow(0.8, i));
			CATransform3D scale = CATransform3DConcat(CATransform3DMakeScale(value, value, 1.0f), CATransform3DMakeAffineTransform(self.transform));
			
			[values addObject:[NSValue valueWithCATransform3D:scale]];
			if (abs(i) % 2 == 1)
			{
				[timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
			}
			else
			{
				[timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
			}
		}
	}
	else if (self.animationType == MGAlertViewAnimationTypeZoomOut)
	{
		CATransform3D scale1 = CATransform3DConcat(CATransform3DMakeScale(1.2f, 1.2f, 1.2f), CATransform3DMakeAffineTransform(self.transform));
		CATransform3D scale2 = CATransform3DConcat(CATransform3DMakeScale(1.0f, 1.0f, 1.0f), CATransform3DMakeAffineTransform(self.transform));
		
		[values addObject:[NSValue valueWithCATransform3D:scale1]];
		[values addObject:[NSValue valueWithCATransform3D:scale2]];
		
		[timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
	}
	else if (self.animationType == MGAlertViewAnimationTypeZoomIn)
	{
		CATransform3D scale1 = CATransform3DConcat(CATransform3DMakeScale(0.1f, 0.1f, 0.1f), CATransform3DMakeAffineTransform(self.transform));
		CATransform3D scale2 = CATransform3DConcat(CATransform3DMakeScale(1.0f, 1.0f, 1.0f), CATransform3DMakeAffineTransform(self.transform));
		
		[values addObject:[NSValue valueWithCATransform3D:scale1]];
		[values addObject:[NSValue valueWithCATransform3D:scale2]];
		
		[timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
	}
	
	// -- scale
	CAKeyframeAnimation *scaleAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
	scaleAnimation.values = values;
	scaleAnimation.timingFunctions = timingFunctions;
	
	CAAnimationGroup *group = [CAAnimationGroup animation];
	group.duration = MGAlertViewShowAnimationDuration;
	group.animations = @[alphaAnimation, scaleAnimation];
	
	self.layer.opacity = 1.0f;
	self.layer.transform = [[values lastObject] CATransform3DValue];
	
	[self.layer addAnimation:group forKey:@"animation"];
}

- (void)hideAnimation
{
	CATransform3D endTransform = CATransform3DConcat(CATransform3DMakeScale(0.7f, 0.7f, 0.7f), CATransform3DMakeAffineTransform(self.transform));
	
	CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
	alphaAnimation.fromValue = @(self.layer.opacity);
	alphaAnimation.toValue = @(0.0f);
	
	CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
	scaleAnimation.fromValue = [NSValue valueWithCATransform3D:self.layer.transform];
	scaleAnimation.toValue = [NSValue valueWithCATransform3D:endTransform];
	
	CAAnimationGroup *group = [CAAnimationGroup animation];
	group.duration = MGAlertViewHideAnimationDuration;
	group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	group.animations = @[alphaAnimation, scaleAnimation];
	group.delegate = self;
	
	self.layer.opacity = 0.0f;
	self.layer.transform = endTransform;
	
	[self.layer addAnimation:group forKey:@"animation"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
	if (flag)
	{
	}
	[[[MGAlertViewContainerWindow sharedInstance] alertViewController] removeAlertView:self];
}

@end
