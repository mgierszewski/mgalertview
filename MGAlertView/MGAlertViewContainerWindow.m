//
//  AlertViewContainer.m
//
//  Created by Maciej Gierszewski on 24.09.2013.
//  Copyright (c) 2013 Maciej Gierszewski All rights reserved.
//
@import QuartzCore;

#import "MGAlertViewContainerWindow.h"
#import "MGAlertViewController.h"
#import "UIImage+MGAlertView.h"

static MGAlertViewContainerWindow *_alertViewContainerInstance = nil;

@interface MGAlertViewContainerWindow ()
@end

@implementation MGAlertViewContainerWindow
{
}

+ (MGAlertViewContainerWindow *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _alertViewContainerInstance = [MGAlertViewContainerWindow new];
    });
    return _alertViewContainerInstance;
}

- (instancetype)init
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    
    self = [super initWithFrame:mainScreen.bounds];
    if (self)
    {
        self.windowLevel = UIWindowLevelStatusBar - 1;
        self.backgroundColor = [UIColor clearColor];
        
        // UIWindowLevelNormal = 0
        // UIWindowLevelStatusBar = 1000.0
        // UIWindowLevelAlert = 2000.0

        self.alertViewController = [MGAlertViewController new];
        self.alertViewController.parentWindow = self;
        
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillChangeStatusBarOrientationNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
            self.frame = mainScreen.bounds;
        }];
    }
    return self;
}

//- (void)layoutSubviews
//{
//    [super layoutSubviews];
//    
//    self.rootViewController.view.frame = self.bounds;
//    [self.rootViewController.view layoutIfNeeded];
//}

//- (void)setRootViewController:(UIViewController *)rootViewController
//{
//    if ([rootViewController isKindOfClass:[MGAlertViewController class]])
//    {
//        [super setRootViewController:rootViewController];
//    }
//}

- (void)makeKeyAndVisible
{
	CGRect keyWindowBounds = [[[UIApplication sharedApplication] keyWindow] bounds];
	
    self.rootViewController = self.alertViewController;
	self.frame = (CGRect){CGPointZero, keyWindowBounds.size};
	
    [super makeKeyAndVisible];
}

@end
