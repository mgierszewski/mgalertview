//
//  UIView+MGAlertView.h
//  MGAlertView
//
//  Created by Maciej Gierszewski on 16.04.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MGAlertView)
- (void)findViewOfClass:(Class)aClass foundBlock:(void (^)(UIView *view))block;
- (void)findViewConformingProtocol:(Protocol *)protocol foundBlock:(void (^)(id view))block;
- (UIView<UITextInput>*)findActiveTextInput;
@end
