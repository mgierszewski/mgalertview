//
//  NSString+MGAlertView.m
//
//  Created by Maciej Gierszewski on 14.04.2014.
//  Copyright (c) 2014 Maciej Gierszewski. All rights reserved.
//

#import "NSString+MGAlertView.h"

@implementation NSString (MGAlertView)

- (CGSize)sizeWithFont:(UIFont *)font maximumWidth:(CGFloat)width
{
    CGSize size = CGSizeZero;
    CGSize allowedSize = CGSizeMake(width == 0.0f ? INFINITY : width, INFINITY);
    
    if ([self respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)])
    {
        CGRect rect = [self boundingRectWithSize:allowedSize
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName: font}
                                         context:nil];
        size = rect.size;
    }
    else
    {
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wdeprecated-declarations"
        size = [self sizeWithFont:font
                constrainedToSize:allowedSize
                    lineBreakMode:NSLineBreakByWordWrapping];
        #pragma clang diagnostic pop
    }
    
    size.width = ceilf(size.width);
    size.height = ceilf(size.height);
    
    return size;
}

@end
