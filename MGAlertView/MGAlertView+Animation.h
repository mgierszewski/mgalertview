//
//  MGAlertView+Animation.h
//  MGAlertView
//
//  Created by Maciek Gierszewski on 22.10.2015.
//  Copyright © 2015 Maciej Gierszewski. All rights reserved.
//

#import <MGAlertView/MGAlertView.h>

static NSTimeInterval const MGAlertViewShowAnimationDuration = 0.3;
static NSTimeInterval const MGAlertViewHideAnimationDuration = 0.3;

@interface MGAlertView (Animation)
- (void)showAnimation;
- (void)hideAnimation;
@end
