//
//  Utils.c
//
//  Created by Maciej Gierszewski on 28.09.2013.
//  Copyright (c) 2013 Maciej Gierszewski. All rights reserved.
//

#import "MGAlertViewUtils.h"

void DebugLog(NSString *format, ...)
{
    #ifdef DEBUG
    
    va_list args;
    va_start(args, format);
    NSLogv(format, args);
    va_end(args);
    
    #endif
}

CFAbsoluteTime MeasureBlock(void (^block)(void))
{
    CFAbsoluteTime start = CFAbsoluteTimeGetCurrent();
    block();
    CFAbsoluteTime end = CFAbsoluteTimeGetCurrent();
    
    return end - start;
}