//
//  MGAlertView.h
//
//  Created by Maciej Gierszewski on 05.03.2013.
//  Copyright (c) 2013 Maciej Gierszewski All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+MGAlertView.h"

#ifndef MGALERTVIEW_CONSTANTS_
#define MGALERTVIEW_CONSTANTS_

//#define MGAlertViewUserDrawViewHierarchyIfPossible
//#define MGAlertViewShowFPS

#endif

typedef NS_ENUM(NSInteger, MGAlertViewAnimationType)
{
    MGAlertViewAnimationTypeBounce,
    MGAlertViewAnimationTypeZoomOut,
    MGAlertViewAnimationTypeZoomIn,
};

typedef NS_ENUM(NSInteger, MGAlertViewBackgroundBlurType)
{
    MGAlertViewBackgroundBlurTypeNone,
    MGAlertViewBackgroundBlurTypeAlert,
    MGAlertViewBackgroundBlurTypeWindow,
};

/**
 *  Delegate
 */
@class MGAlertView;
@protocol MGAlertViewDelegate <NSObject>
@optional
- (BOOL)alertView:(MGAlertView *)alertView shouldHideWithButtonIndex:(NSInteger)index;
- (void)alertView:(MGAlertView *)alertView clickedButtonAtIndex:(NSInteger)index;
@end

/**
 *  MGAlertView
 */
@interface MGAlertView : UIView <UIAppearance>
@property (assign) id<MGAlertViewDelegate> delegate;
@property (nonatomic, strong) NSDictionary *userInfo;

//@property (nonatomic, assign) CGSize maximumSize;

// ---------------------
// appearance properties
// ---------------------
@property (nonatomic, assign) UIEdgeInsets alertMargins;

@property (nonatomic, strong) UIColor *alertBackgroundColor                         UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat cornerRadius                                  UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat alertBackgroundImageAlpha                     UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) UIImage *alertBackgroundImage                         UI_APPEARANCE_SELECTOR;

// buttons
@property (nonatomic, strong) UIFont *buttonFont                                    UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *buttonTitleColor                             UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *buttonHighlightedTitleColor                  UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *buttonBackgroundColor                        UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *buttonHighlightedBackgroundColor             UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *buttonBorderColor                            UI_APPEARANCE_SELECTOR;

@property (nonatomic, assign) UIEdgeInsets buttonGroupMargins                       UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) UIEdgeInsets buttonContentInsets                      UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat buttonCornerRadius                            UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat buttonBorderWidth                             UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat buttonHorizontalSpacing                       UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat buttonVerticalSpacing                         UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *buttonSeparatorColor                         UI_APPEARANCE_SELECTOR;

// confirmation button (the last one)
@property (nonatomic, strong) UIFont *confirmationButtonFont                        UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *confirmationButtonTitleColor                 UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *confirmationButtonHighlightedTitleColor      UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *confirmationButtonBackgroundColor            UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *confirmationButtonHighlightedBackgroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *confirmationButtonBorderColor                UI_APPEARANCE_SELECTOR;

@property (nonatomic, assign) NSInteger lastButtonIsConfirmationButton              UI_APPEARANCE_SELECTOR;

// title
@property (nonatomic, strong) UIColor *titleTextColor                               UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIFont *titleFont                                     UI_APPEARANCE_SELECTOR;

// message
@property (nonatomic, strong) UIColor *messageTextColor                             UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIFont *messageFont                                   UI_APPEARANCE_SELECTOR;

// content
@property (nonatomic, assign) UIEdgeInsets contentMargins                           UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat contentVerticalSpacing                        UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) CGFloat contentHorizontalSpacing                      UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *contentButtonSeparatorColor                  UI_APPEARANCE_SELECTOR;

@property (nonatomic, assign) MGAlertViewAnimationType animationType                UI_APPEARANCE_SELECTOR;

// --------
// subviews
// --------
@property (nonatomic, readonly) UILabel *titleLabel;
@property (nonatomic, readonly) UILabel *messageLabel;
@property (nonatomic, readonly) UIView *customView;
@property (nonatomic, readonly) NSInteger numberOfButtons;

- (NSString *)buttonTitleAtIndex:(NSInteger)index;

// -----------
// constructor
// -----------
- (MGAlertView *)initWithTitle:(NSString *)title message:(NSString *)message image:(UIImage *)image delegate:(id<MGAlertViewDelegate>)delegate buttons:(NSArray *)buttonTitles;
- (MGAlertView *)initWithTitle:(NSString *)title message:(NSString *)message customView:(UIView *)view delegate:(id<MGAlertViewDelegate>)delegate buttons:(NSArray *)buttonTitles;

// deprecated constructors
- (MGAlertView *)initWithMessage:(NSString *)message image:(UIImage *)image delegate:(id<MGAlertViewDelegate>)delegate buttons:(NSArray *)buttonTitles __deprecated;
- (MGAlertView *)initWithMessage:(NSString *)message customView:(UIView *)view delegate:(id<MGAlertViewDelegate>)delegate buttons:(NSArray *)buttonTitles __deprecated;

// -----------
// show / hide
// -----------
- (void)show:(BOOL)animated;
- (void)hide:(BOOL)animated;
- (void)hide:(BOOL)animated afterDelay:(NSTimeInterval)delay;
@end

#pragma mark - Static

@interface MGAlertView (Static)

+ (void)setBackgroundBlurType:(MGAlertViewBackgroundBlurType)type;
+ (MGAlertViewBackgroundBlurType)backgroundBlurType;

+ (void)setBlurBoxSize:(int)blurBoxSize;
+ (int)blurBoxSize;

+ (void)setWindowBackgroundColor:(UIColor *)color;
+ (UIColor *)windowBackgroundColor;

// creators
+ (MGAlertView *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message customView:(UIView *)customView delegate:(id<MGAlertViewDelegate>)delegate buttons:(NSArray *)buttons;
+ (MGAlertView *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message customView:(UIView *)customView;
+ (MGAlertView *)showAlertViewWithTitle:(NSString *)title message:(NSString *)message;
+ (MGAlertView *)showAlertViewWithMessage:(NSString *)message customView:(UIView *)customView;
+ (MGAlertView *)showAlertViewWithCustomView:(UIView *)customView;
+ (MGAlertView *)showAlertViewWithMessage:(NSString *)message;
+ (MGAlertView *)showAlertViewWithImage:(UIImage *)image;
+ (MGAlertView *)showAlertViewWithMessage:(NSString *)message image:(UIImage *)image;
+ (MGAlertView *)showAlertViewWithLoader;

+ (MGAlertView *)showFlashAlertViewWithImage:(UIImage *)image;
+ (MGAlertView *)showFlashAlertViewWithMessage:(NSString *)message;
+ (MGAlertView *)showFlashAlertViewWithMessage:(NSString *)message image:(UIImage *)image;
+ (MGAlertView *)showFlashAlertViewWithTitle:(NSString *)title message:(NSString *)message;

// deprecated
+ (void)setBlurredBackgroundEnabled:(BOOL)enabled   __deprecated_msg("use setBackgroundBlurType: instead");
+ (BOOL)blurredBackgroundEnabled                    __deprecated_msg("use backgroundBlurType instead");

+ (NSArray *)visibleAlerts;
@end
