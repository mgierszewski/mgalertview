//
//  Utils.h
//
//  Created by Maciej Gierszewski on 28.09.2013.
//  Copyright (c) 2013 Maciej Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef MGAlertViewUtils_h_
#define MGAlertViewUtils_h_

extern void DebugLog(NSString *format, ...);
extern CFAbsoluteTime MeasureBlock(void (^block)(void));

#endif
