//
//  UIView+MGAlertView.m
//
//  Created by Maciej Gierszewski on 16.04.2014.
//  Copyright (c) 2014 Maciej Gierszewski. All rights reserved.
//

#import "UIView+MGAlertView.h"

@implementation UIView (MGAlertView)

- (void)findViewOfClass:(Class)aClass foundBlock:(void (^)(UIView *view))block
{
    if ([self isKindOfClass:aClass])
    {
        if (block)
        {
            block(self);
        }
    }
    for (UIView *subview in self.subviews)
    {
        [subview findViewOfClass:aClass foundBlock:block];
    }
}

- (void)findViewConformingProtocol:(Protocol *)protocol foundBlock:(void (^)(id))block
{
    if ([self conformsToProtocol:protocol])
    {
        if (block)
        {
            block(self);
        }
    }
    for (UIView *subview in self.subviews)
    {
        [subview findViewConformingProtocol:protocol foundBlock:block];
    }
}

- (UIView<UITextInput>*)findActiveTextInput
{
    if ([self isFirstResponder] && [self conformsToProtocol:@protocol(UITextInput)])
    {
        return (UIView<UITextInput> *)self;
    }
    
    for (UIView *subview in self.subviews)
    {
        UIView<UITextInput> *firstResponder = [subview findActiveTextInput];
        if (firstResponder)
        {
            return firstResponder;
        }
    }
    return nil;
}

@end
