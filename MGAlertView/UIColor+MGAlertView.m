//
//  UIColor.m
//
//  Created by Maciej Gierszewski on 19.09.2013.
//  Copyright (c) 2013 Maciej Gierszewski. All rights reserved.
//

#import "UIColor+MGAlertView.h"

@implementation UIColor (MGAlertView)

+ (UIColor *) colorWithHex:(int)hexColor
{
    return [UIColor colorWithHex:hexColor alpha:1.0];
}

+ (UIColor *) colorWithHex:(int)hexColor alpha:(float)alpha
{
    float red = ((hexColor) & 0xFF0000) >> 16;
    float green = ((hexColor) & 0x00FF00) >> 8;
    float blue = ((hexColor) & 0x0000FF) >> 0;
    
    return [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha];
}

@end