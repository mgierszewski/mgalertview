//
//  UIColor.h
//
//  Created by Maciej Gierszewski on 19.09.2013.
//  Copyright (c) 2013 Maciej Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MGAlertView)

+ (UIColor *) colorWithHex:(int)hexColor;
+ (UIColor *) colorWithHex:(int)hexColor alpha:(float)alpha;

@end