//
//  AppDelegate.h
//  MGAlertViewTest
//
//  Created by Maciej Gierszewski on 19.02.2014.
//  Copyright (c) 2014 Maciej Gierszewski All rights reserved.
//

#import <UIKit/UIKit.h>;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
