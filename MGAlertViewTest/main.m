//
//  main.m
//  MGAlertViewTest
//
//  Created by Maciej Gierszewski on 19.02.2014.
//  Copyright (c) 2014 Maciej Gierszewski All rights reserved.
//

#import <UIKit/UIKit.h>;

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
