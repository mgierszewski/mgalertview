//
//  ViewController.m
//  MGAlertViewTest
//
//  Created by Maciej Gierszewski on 19.02.2014.
//  Copyright (c) 2014 Maciej Gierszewski All rights reserved.
//

#import "ViewController.h"
#import "MGAlertView/MGAlertView.h"

@interface ViewController () <MGAlertViewDelegate, UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
- (IBAction)textFieldEditingChanged:(UITextField *)textField;
- (IBAction)textFieldDidEndOnExit:(UITextField *)textField;

- (IBAction)buttonTouchedUp:(id)sender;
- (IBAction)button2TouchedUp:(id)sender;
@end

@implementation ViewController
{
    UITextField *_textField;
    UITextField *_textField2;
    UIButton *_button;
    UIView *_textFieldContainer;
    UIActivityIndicatorView *_loader;
    MGAlertView *_loaderAlertView;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [MGAlertView setBackgroundBlurType:MGAlertViewBackgroundBlurTypeAlert];
    [MGAlertView setBlurBoxSize:31];
    [[MGAlertView appearance] setAlertBackgroundImageAlpha:0.3];
    
//    [MGAlertView showAlertViewWithMessage:@"text"];
	// Do any additional setup after loading the view, typically from a nib.
    
    _loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [_loader startAnimating];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 240.0f, 32.0f)];
    _textField.backgroundColor = [UIColor whiteColor];
    
    _textField2 = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 40.0f, 240.0f, 32.0f)];
    _textField2.backgroundColor = [UIColor whiteColor];
    
    _button = [UIButton buttonWithType:UIButtonTypeSystem];
    _button.frame = CGRectMake(0.0f, 80.0f, 240.0f, 32.0f);
    [_button addTarget:self action:@selector(button2TouchedUp:) forControlEvents:UIControlEventTouchUpInside];
    [_button setTitle:@"Button" forState:UIControlStateNormal];
    
    _textFieldContainer = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 240.0f, 112.0f)];
    _textFieldContainer.backgroundColor = [UIColor clearColor];
    [_textFieldContainer addSubview:_textField];
    [_textFieldContainer addSubview:_textField2];
    [_textFieldContainer addSubview:_button];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"title" message:@"message" delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles:@"button1", @"button2", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
//   [alertView show];
    
    [MGAlertView showAlertViewWithMessage:@"text"];
//        [MGAlertView showAlertViewWithMessage:@"text2"];
}

- (IBAction)textFieldDidEndOnExit:(UITextField *)textField
{
    [textField resignFirstResponder];
}

- (IBAction)textFieldEditingChanged:(UITextField *)textField
{
    if (textField.text.length > 0)
    {
        [MGAlertView showAlertViewWithMessage:@"Message"];
    }
}

- (IBAction)button2TouchedUp:(UIButton *)sender
{
    UIImagePickerController *imagePickerController = [UIImagePickerController new];
    imagePickerController.delegate = self;
    [sender.window.rootViewController presentViewController:imagePickerController animated:YES completion:^{
        
    }];
}

- (IBAction)buttonTouchedUp:(UIButton *)sender
{
    // NSString *longText = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc iaculis felis sed metus elementum, at faucibus urna porttitor. Nam quis egestas purus, vel egestas est. Nulla commodo sapien sed nibh mattis, a porttitor orci tempus. Pellentesque sodales fermentum dignissim. Nullam. Nulla commodo sapien sed nibh mattis, a porttitor orci tempus. ";
    MGAlertView *alertView = [[MGAlertView alloc] initWithTitle:@"Title"
                                                        message:@"Short text"
                                                     customView:_textFieldContainer
                                                       delegate:self
                                                        buttons:@[ @"Mapy", @"Google Maps", @"Anuluj" ]];
    alertView.animationType = MGAlertViewAnimationTypeZoomIn;
    [alertView show:YES];
}

- (void)alertView:(MGAlertView *)alertView clickedButtonAtIndex:(NSInteger)index
{
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [_loaderAlertView hide:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
